﻿// Copyright 2020-2021 CesiumGS, Inc. and Contributors

#include "CesiumTileMapServiceRasterOverlay.h"
#include "Cesium3DTilesSelection/TileMapServiceRasterOverlay.h"
#include "CesiumRuntime.h"

UCesiumTileMapServiceRasterOverlay::UCesiumTileMapServiceRasterOverlay()
{
    Headers.Add(TEXT("Accept"), TEXT("text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7"));
}

std::unique_ptr<Cesium3DTilesSelection::RasterOverlay>
UCesiumTileMapServiceRasterOverlay::CreateOverlay(
    const Cesium3DTilesSelection::RasterOverlayOptions& options) {
  Cesium3DTilesSelection::TileMapServiceRasterOverlayOptions tmsOptions;
  if (MaximumLevel > MinimumLevel && bSpecifyZoomLevels) {
    tmsOptions.minimumLevel = MinimumLevel;
    tmsOptions.maximumLevel = MaximumLevel;
  }
  std::vector<CesiumAsync::IAssetAccessor::THeader> headers;
  for (auto& Header : Headers)
  {
      CesiumAsync::IAssetAccessor::THeader header;
      header.first = TCHAR_TO_UTF8(*Header.Key);
      header.second = TCHAR_TO_UTF8(*Header.Value);
      headers.push_back(header);
  }
  return std::make_unique<Cesium3DTilesSelection::TileMapServiceRasterOverlay>(
      TCHAR_TO_UTF8(*this->MaterialLayerKey),
      TCHAR_TO_UTF8(*this->Url),
      headers,
      tmsOptions,
      options);
}
